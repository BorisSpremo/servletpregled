<%-- 
    Document   : index
    Created on : Jun 18, 2020, 9:33:08 AM
    Author     : Spremo
--%>

<%@page import="java.sql.*" %>
<% Class.forName("com.mysql.jdbc.Driver"); %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ucenici</title>
    </head>
    <body>
        <h1>Ucenici</h1>
        <%!
        public class Ucenik {
            String URL = "jdbc:mysql://localhost:3306/skola" ;
            String USER = "magic";
            String PASS = "skola27";
            
            Connection conn= null;
            PreparedStatement psSkola = null;
            ResultSet rsSkola = null;

        public Ucenik(){
            try{
                conn = DriverManager.getConnection(URL, USER, PASS);
                psSkola = conn.prepareStatement("SELECT Id, Ime, Prezime, email, Adresa, PostanskiBroj, Mesto FROM polaznik");
            } catch (SQLException e){ e.printStackTrace(); }
        }
        public ResultSet uzmiUcenik () {
            try {
                rsSkola = psSkola.executeQuery();
            } catch ( SQLException e ) { e.printStackTrace(); }
            return rsSkola;
        }
        }
        %>
        <%
        Ucenik ucenik = new Ucenik();
        ResultSet ucenici = ucenik.uzmiUcenik();
        %>
    </body>
    <table border="1">
        
        <tbody>
            <tr>
                <td>rb</td>
                <td>ime</td>
                <td>prezime</td>
                <td>adresa</td>
                <td>postanski broj</td>
                <td>mesto</td>
                <td>e-mail</td>
            </tr>
            <% while ( ucenici.next() ) {%>
                <tr>
                    <td><%= ucenici.getInt("Id") %></td>
                    <td><%=  ucenici.getString("Ime")%></td>
                    <td><%= ucenici.getString("Prezime")%></td>
                    <td><%= ucenici.getString("Adresa")%></td>
                    <td><%= ucenici.getInt("PostanskiBroj")%></td>
                    <td><%= ucenici.getString("Mesto")%></td>
                    <td><%= ucenici.getString("email")%></td>
                </tr>
            <% } %>
        </tbody>
    </table>

</html>
